# Profile runs on login.

[ -f $HOME/.bashrc ] && . $HOME/.bashrc

# Adds '~/.local/bin' to PATH
export PATH="$PATH:$HOME/.local/bin"

# Home directory clean up.
export HISTFILE="$HOME/.cache/bash_history"
export LESSHISTFILE="-"
export INPUTRC="$HOME/.cache/inputrc"

# Default Programs.
export EDITOR="vim"
export TERMINAL="urxvt"
export BROWSER="firefox"
export READER="zathura"
export FILE="nnn"

